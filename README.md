# RackN Backstage Tutorial

This tutorial will guide you in setting up basic
[Backstage](https://backstage.io/) integration with the
[RackN Digital Rebar Platform](https://rackn.com/).

You will be creating a simple backend plugin to provide functionality to the
template and frontend plugins you will create afterward. The end goal is a
simple integration that will allow you to quickly **spin up**, **view**, and
**destroy** Clusters in DRP.

It is composed of three parts:

1. The [backend](backend.md) plugin, a requirement for the following two parts,
2. The [scaffolder](scaffolder.md), a sample template to spin up DRP Clusters,
   and
3. The [frontend](frontend.md) plugin, a view for listing Clusters and deleting
   them.
