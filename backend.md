# Backend

The backend plugin is responsible for providing custom actions to the
[scaffolder] and API functionality to the [frontend].

In this tutorial, we will be setting up the aforementioned plugin to work with
our two other components.

We will:

<!-- prettier-ignore -->
1. [Create the plugin](#creating-the-plugin)
2. Optionally [add custom actions](#adding-custom-actions) for the [scaffolder]
3. Optionally [add router extensions](#adding-router-extensions) for the [frontend]
4. [Export our plugin changes](#exporting-our-plugin-functionality)
5. [Authorize our Backstage instance with DRP](#drp-authorization)
6. [Register the plugin in Backstage](#register-the-plugin)

**While you can choose to omit adding custom actions or router extensions, it is
recommended that you do both so that you get a full sense for Digital Rebar's
power with Backstage.**

**Note:** A sample, reference backend plugin is available at
<https://gitlab.com/zfranks/backstage-plugin-digitalrebar-backend>.

## Creating the plugin

The official documentation for creating a backend plugin is available
[here](https://backstage.io/docs/plugins/backend-plugin), but we will walk
through it for our cases.

Start from the root directory of your Backstage instance.

```sh
yarn new --select backend-plugin
```

Let's name (ID) our plugin `digitalrebar`. This will create a package at
`plugins/digitalrebar-backend`. The package will be named
`@internal/plugin-digitalrebar-backend`. You are free to rename these after this
tutorial.

## Using the Digital Rebar TypeScript API

From the root of your Backstage instance, issue the following command to add the
DRP TS API to your plugin.

```sh
yarn add --cwd plugins/digitalrebar-backend @rackn/digitalrebar-api
```

This will add the official
[Digital Rebar TypeScript API](https://www.npmjs.com/package/@rackn/digitalrebar-api)
to your backend plugin. It can be used as a lightweight wrapper for DRP REST API
calls, which will greatly simplify the implementation of our
[custom actions](#adding-custom-actions) and the functionality our [frontend]
should present to the user.

## Adding custom actions

First, open up your Backstage instance in the text editor of your choice. In
your plugin directory at `plugins/digitalrebar-backend`, create a new directory
`actions`:

```sh
mkdir plugins/digitalrebar-backend/actions
```

Create a new file `clusters.ts` in this folder. It will hold the functionality
for our `drp:clusters:create` action we will use in the [scaffolder].

Set `clusters.ts` to the following. We will break it down after the source.

```ts
import { Config } from '@backstage/config';
import type { JsonValue } from '@backstage/types';
import { createTemplateAction } from '@backstage/plugin-scaffolder-backend';
import DRApi, { DRMachine } from '@rackn/digitalrebar-api';

export const clustersActions = (config: Config) => {
  // DRP endpoints use self-signed certificates
  // you may want to set this manually, but it is here
  // for development purposes
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

  return [
    createTemplateAction({
      id: 'drp:clusters:create',
      schema: {
        input: {
          type: 'object',
        },
      },
      handler: async (ctx) => {
        const endpoint = config.getString('digitalrebar.endpoint');
        const token = config.getString('digitalrebar.token');
        const api = new DRApi(endpoint);
        api.setToken(token);

        ctx.logger.info('Creating cluster...');

        const response = await api.clusters.create(
          ctx.input as unknown as DRMachine
        );

        ctx.logger.info(`Created cluster with UUID ${response.data.Uuid}`);
        ctx.output('object', response.data as unknown as JsonValue);
        ctx.output('endpoint', endpoint);
      },
    }),
  ];
};
```

Ignoring our import section, let's look at the entire thing, broken down:

<br />

```ts
export const clustersActions = (config: Config) => {
  // DRP endpoints use self-signed certificates
  // you may want to set this manually, but it is here
  // for development purposes
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
```

This section sets the `NODE_TLS_REJECT_UNAUTHORIZED` environment variable to
`0`, as DRP endpoints use self-signed certificates. You may want to set this
elsewhere, or remove it entirely if your DRP endpoint is properly signed.

<br />

```ts
  return [
    createTemplateAction({
      id: 'drp:clusters:create',
      schema: {
        input: {
          type: 'object',
        },
      },
```

We return an array of actions. In this case, we are only creating one action.
Actions are created using the Backstage-provided `createTemplateAction`. We will
call it `drp:clusters:create`. Its input schema is simply an object with no
known parameters; it simply gets shipped to our DRP API as the object we are
trying to create. In this way, the input object of this action can be an entire
DRP Cluster.

<br />

```ts
      handler: async (ctx) => {
        const endpoint = config.getString('digitalrebar.endpoint');
        const token = config.getString('digitalrebar.token');
        const api = new DRApi(endpoint);
        api.setToken(token);
```

In this section, we grab the `endpoint` and `token` config options from our
`app-config.yaml` (see the section on [DRP authorization](#drp-authorization))
and we give them to our `DRApi` so that we can make calls to our DRP endpoint.

<br />

<!-- prettier-ignore -->
```ts
        ctx.logger.info('Creating cluster...');

        const response = await api.clusters.create(
          ctx.input as unknown as DRMachine
        );
```

Here, we note to the user that we are creating the cluster. Then, we call
`api.clusters.create` with our input object.

Note the TypeScript that is happening here (`as unknown as DRMachine`).
Backstage provides `input` as a `JsonValue`, which cannot be properly cast to
`DRMachine`. We trust the conversion, so we assume `input` is `unknown`, and
then `DRMachine`, so that the value can be passed into the `create` method.

<br />

<!-- prettier-ignore -->
```ts
        ctx.logger.info(`Created cluster with UUID ${response.data.Uuid}`);
        ctx.output('object', response.data as unknown as JsonValue);
        ctx.output('endpoint', endpoint);
```

We notify the user that the cluster has been created, and its UUID is fetched
from `response.data.Uuid`. The entire Cluster object that was created is
available at `response.data`, for that matter.

We set our outputs:

- `object` is our `response.data` described above. Note again the TypeScript
  casting that is used for the inverse case.
- `endpoint` is our endpoint we got from our configuration a few sections above.
  Again, see the section on [DRP authorization](#drp-authorization).

## Adding router extensions

In order to provide functionality to the [frontend], we need to extend
Backstage's router in our backend plugin.

Create a new folder `service` in your backend plugin's directory if it does not
exist:

```sh
mkdir plugins/digitalrebar-backend/service
```

Then, create a file `router.ts` in it and set it to the following. Again, we
will break it down afterwards.

There is a chance this file already exists for you in your boilerplate plugin.
If it does, make sure to add the relevant parts discussed below.

```ts
import { errorHandler } from '@backstage/backend-common';
import { Config } from '@backstage/config';
import DRApi, { DRWorkOrder } from '@rackn/digitalrebar-api';
import express from 'express';
import Router from 'express-promise-router';
import { Logger } from 'winston';

export interface RouterOptions {
  logger: Logger;
  config: Config;
}

export async function createRouter(
  options: RouterOptions
): Promise<express.Router> {
  const { logger } = options;

  const router = Router();
  router.use(express.json());

  const endpoint = options.config.getString('digitalrebar.endpoint');
  const token = options.config.getString('digitalrebar.token');

  const api = new DRApi(endpoint);
  api.setToken(token);

  router.get('/health', (_, response) => {
    logger.info('PONG!');
    response.json({ status: 'ok' });
  });

  router.get('/clusters', async (_, response) => {
    response.json((await api.clusters.list({ aggregate: 'true' })).data);
  });

  router.delete('/clusters/:uuid', async (request, response) => {
    response.json((await api.clusters.delete(request.params.uuid)).data);
  });

  router.use(errorHandler());
  return router;
}
```

Much of this is Backstage boilerplate, and is somewhat explained in the
[official documentation](https://backstage.io/docs/plugins/backend-plugin#developing-your-backend-plugin).
The parts we care about are below:

<br />

<!-- prettier-ignore -->
```ts
  const endpoint = options.config.getString('digitalrebar.endpoint');
  const token = options.config.getString('digitalrebar.token');

  const api = new DRApi(endpoint);
  api.setToken(token);
```

This should look familiar if you completed the
[custom actions](#adding-custom-actions) part of this tutorial.

<br />

<!-- prettier-ignore -->
```ts
  router.get('/clusters', async (_, response) => {
    response.json((await api.clusters.list({ aggregate: 'true' })).data);
  });
```

This call adds a listener to `GET /clusters` to our router. It returns a list of
Clusters from our DRP endpoint, thanks to the `api.clusters.list` method
provided by the [DRP TypeScript API](#using-the-digital-rebar-typescript-api).
We also set `aggregate: 'true'` here so that DRP responds with _all_ Params,
even those across inherited Profiles, as all Clusters have an associated
Profile.

<br />

<!-- prettier-ignore -->
```ts
  router.delete('/clusters/:uuid', async (request, response) => {
    response.json((await api.clusters.delete(request.params.uuid)).data);
  });
```

Like the call above, this will add a listener to `DELETE /clusters/:uuid`. It
will be responsible for deleting clusters given their UUID.

Our plugin is now prepared to listen to API calls from our [frontend].

## Exporting our plugin functionality

To make our backend functionality visible to the rest of Backstage, set your
plugin's `index.ts` to the following:

```ts
// Export our cluster actions
export * from './actions/clusters.ts';

// Export our router extensions for the frontend
export * from './service/router.ts';
```

## DRP Authorization

In order for your Backstage instance to be able to make requests to DRP, you
need to add some information to your instance's `app-config.yaml`.

First, you'll need to get your endpoint. Your endpoint should be the IP address
followed by the port DRP listens to (by default, `8092`). It is the same as the
IP and port you use to log into the DRP UX. You'll replace `YOUR-ENDPOINT` in
the following `yaml` excerpt with this address.

Then, you'll need a DRP user's token that the plugin will make requests through.
Log in to a user on your DRP instance through the
[portal](https://portal.rackn.io/), and click `Copy Auth Token` at the very
bottom of the left sidebar. You'll replace `YOUR-TOKEN` in the following with
this.

Open `app-config.yaml` in the text editor of your choice, and add the following
at the top-level somewhere in the file:

```yaml
digitalrebar:
  endpoint: YOUR-ENDPOINT
  token: YOUR-TOKEN
```

## Register the plugin

To register the plugin with Backstage, you'll have to create and modify a few
files.

### If you made router extensions

Start by creating `packages/backend/src/plugins/digitalrebar.ts` and setting it
to the following.

```ts
import { createRouter } from '@internal/plugin-digitalrebar-backend';
import { Router } from 'express';
import { PluginEnvironment } from '../types';

export default async function createPlugin(
  env: PluginEnvironment
): Promise<Router> {
  return await createRouter({ logger: env.logger, config: env.config });
}
```

Then, open `packages/backend/src/index.ts`, and add:

```ts
// imports ...
import digitalrebar from './plugins/digitalrebar'; // add this line

// down a bit, in the main function ...
async function main() {
  // ... more environments here ...
  const appEnv = useHotMemoize(module, () => createEnv('app'));
  const drpEnv = useHotMemoize(module, () => createEnv('drp')); // add this line

  // ...
  apiRouter.use('/search', await search(searchEnv));
  apiRouter.use('/drp', await digitalrebar(drpEnv)); // add this line

  // ...
}
```

This will register the plugin router with Backstage's API router.

### If you added custom actions

Add the following to `packages/backend/src/plugins/scaffolder.ts`:

```ts
import { clustersActions } from '@internal/plugin-digitalrebar-backend';

export default async function createPlugin(
  env: PluginEnvironment
): Promise<Router> {
  const catalogClient = new CatalogClient({
    discoveryApi: env.discovery,
  });

  const integrations = ScmIntegrations.fromConfig(env.config);

  // since we are adding actions, we must manually create the built-in actions
  // if you are already adding custom actions, simply add the `...clustersActions` line
  const actions = [
    ...createBuiltinActions({
      catalogClient,
      integrations,
      config: env.config,
      reader: env.reader,
    }),
    // add this following line
    ...clustersActions(env.config),
  ];

  return await createRouter({
    logger: env.logger,
    config: env.config,
    database: env.database,
    reader: env.reader,
    catalogClient,
    identity: env.identity,
    actions, // add this line if you were not using custom actions before
  });
}
```

## Usage

As an independent component, the backend plugin does not do much. That said,
continue on to the [scaffolder] (if you
[added custom actions](#adding-custom-actions)) or to the [frontend] (if you
[added router extensions](#adding-router-extensions)). If you did both, choose
either!

[scaffolder]: scaffolder.md
[frontend]: frontend.md
