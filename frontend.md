# Frontend

The frontend plugin is used to display information in the Backstage portal, and
uses the [backend] plugin to communicate with Digital Rebar.

In this tutorial, we will create and set up the frontend plugin so that we can
view a list of Clusters in our DRP endpoint, as well as a way to delete
clusters.

If you have not yet completed the [backend] tutorial, please complete that
first, as there is functionality required by the frontend!

This tutorial assumes a basic understanding of [React](https://reactjs.org/).

**Note:** A sample, reference frontend plugin is available at
<https://gitlab.com/zfranks/backstage-plugin-digitalrebar>.

## Creating the plugin

The official documentation for creating a frontend plugin is available
[here](https://backstage.io/docs/plugins/backend-plugin), but we will walk
through it for our cases.

Start from the root directory of your Backstage instance.

```sh
yarn new --select plugin
```

Use the same ID as our [backend] plugin, `digitalrebar`.

This will create a package at `plugins/digitalrebar`. The package will be named
`@internal/plugin-digitalrebar`. You are free to rename these after this
tutorial.

## Adding the plugin page

Let's start by adding some components.

Create `plugins/digitalrebar/src/components/Clusters/View.tsx` with the
following source:

```tsx
import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import {
  InfoCard,
  Header,
  Page,
  Content,
  ContentHeader,
  HeaderLabel,
  SupportButton,
} from '@backstage/core-components';
import { ClustersTable } from './Table';

export const ClustersView = () => (
  <Page themeId="tool">
    <Header title="Welcome to Digital Rebar!">
      <HeaderLabel label="Owner" value="RackN" />
      <HeaderLabel label="Lifecycle" value="Alpha" />
    </Header>
    <Content>
      <ContentHeader title="Digital Rebar">
        <SupportButton>Some description goes here.</SupportButton>
      </ContentHeader>
      <Grid container spacing={3} direction="column">
        <Grid item>
          <ClustersTable />
        </Grid>
      </Grid>
    </Content>
  </Page>
);
```

This is just React filler that follows Backstage's conventions for plugin pages.
It should be reminiscent of the included boilerplate `ExampleComponent`.

Now, create `plugins/digitalrebar/src/components/Clusters/Table.tsx` with the
following source:

```tsx
import { Progress, Table, TableColumn } from '@backstage/core-components';
import { configApiRef, errorApiRef, useApi } from '@backstage/core-plugin-api';
import Alert from '@material-ui/lab/Alert';
import { DRMachine } from '@rackn/digitalrebar-api';
import React, { useCallback, useMemo, useState } from 'react';
import useAsync from 'react-use/lib/useAsync';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { IconButton, Link } from '@material-ui/core';

const ClusterActions: React.FC<{ data: DRMachine; type: 'row' | 'group' }> = ({
  data,
}) => {
  const config = useApi(configApiRef);
  const [loading, setLoading] = useState(false);
  const [deleted, setDeleted] = useState(false);

  const onDelete = useCallback(() => {
    if (loading) return;
    setLoading(true);

    fetch(
      `${config.getString('backend.baseUrl')}/api/drp/clusters/${data.Uuid}`,
      { method: 'DELETE' }
    )
      .then(() => {
        setDeleted(true);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [config, loading, data.Uuid]);

  if (deleted) return <i>Deleted.</i>;
  if (loading) return <Progress />;

  return (
    <IconButton size="small" onClick={onDelete}>
      <DeleteIcon />
    </IconButton>
  );
};

export const ClustersTable = () => {
  const config = useApi(configApiRef);
  const endpoint = useMemo(
    () => config.getString('digitalrebar.endpoint'),
    [config]
  );

  const { value, loading, error } = useAsync(async (): Promise<DRMachine[]> => {
    return await fetch(
      `${config.getString('backend.baseUrl')}/api/drp/clusters`
    ).then((r) => r.json());
  }, []);

  const columns = useMemo<TableColumn<DRMachine>[]>(
    () => [
      {
        title: 'Name',
        render: (data, _) => (
          <Link
            href={`https://portal.rackn.io/#/e/${endpoint}/clusters/${data.Uuid}`}
          >
            {data.Name}
          </Link>
        ),
      },
      { title: 'Size', field: 'Params.cluster/count' },
      { title: 'Broker', field: 'Params.broker/name' },
      {
        title: 'Actions',
        render: (data, type) => <ClusterActions data={data} type={type} />,
      },
    ],
    [endpoint]
  );

  if (loading) {
    return <Progress />;
  } else if (error) {
    return <Alert severity="error">{error.message}</Alert>;
  }

  return (
    <Table
      title="Clusters"
      options={{ search: false, paging: false }}
      columns={columns}
      data={value ?? []}
    />
  );
};
```

Let's break down each of the components individually.

### `ClusterActions`

This component provides the delete button, allowing us to delete Clusters from
the table. Of its source, most notably is the call to `/api/drp/clusters/:uuid`,
which we added in our [backend] plugin.

### `ClustersTable`

A table of Clusters. Includes some basic information about each cluster, as well
as the `ClusterActions` component allowing us to remove clusters by their row.

`useApi` and `useAsync` are hooks provided by Backstage, as well as the `Table`
component (and its child `TableColumn`s). There is an official
[Backstage tutorial](https://backstage.io/docs/tutorials/quickstart-app-plugin)
that goes into further detail on these pieces.

You'll notice the call to `useAsync` uses the `GET /clusters` route we added in
our [backend] plugin. The preceding `/drp` was declared when we
[registered the backend plugin with Backstage](backend.md#if-you-made-router-extensions).

## Registering the plugin with Backstage

Conveniently, [creating the plugin](#creating-the-plugin) automatically
registers it within Backstage's frontend router, so there are no steps that must
be taken from the perspective of this tutorial.

### Register it in another instance

If you would like to register it with another Backstage instance after
publishing the plugin, check the diffs in `packages/app/src/App.tsx` after
creating the plugin. It should be the addition of an `import` line and a new
`Route` entry.

## Usage

Start your Backstage instance with

```sh
yarn dev
```

and navigate to `http://localhost:3000/digitalrebar` to see your changes.

Optionally, create a Cluster with the template you set up in the [scaffolder]
tutorial, and report back to see if it is now in the frontend table. Check to
see that the delete icon deletes your Cluster.

[backend]: backend.md
[scaffolder]: scaffolder.md
